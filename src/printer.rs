use crate::item::Item;
use lscolors::{LsColors, Style};
use std::io::{self, Write};
use std::os::unix::ffi::OsStrExt;

pub fn print(items: &[Item]) -> io::Result<()> {
    let mut writer = io::BufWriter::with_capacity(8192, io::stdout());
    let lscolors = LsColors::from_env().unwrap_or_default();

    for item in items {
        let style = lscolors.style_for_path(&item.path);
        let ansi_style = style.map(Style::to_ansi_term_style).unwrap_or_default();

        match &item.path.to_str() {
            Some(utf8_str) => writeln!(writer, "{}", ansi_style.paint(*utf8_str))?,
            None => {
                writer.write_all(item.path.as_os_str().as_bytes())?;
                writer.write_all(b"\n")?;
            }
        }
    }
    Ok(())
}
