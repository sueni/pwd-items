use std::path::PathBuf;
use std::time::SystemTime;

pub struct Item {
    pub path: PathBuf,
    pub mtime: SystemTime,
    pub size: u64,
}

impl Item {
    pub fn new(path: PathBuf) -> Self {
        let metadata = path.symlink_metadata().unwrap();

        Self {
            path: path.strip_prefix("./").unwrap_or(&path).to_path_buf(),
            mtime: metadata.modified().unwrap(),
            size: metadata.len(),
        }
    }
}
