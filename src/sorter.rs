use crate::cli::Course;
use crate::cli::SortingMode;
use crate::item::Item;

pub struct Sorter {
    pub items: Vec<Item>,
}

impl Sorter {
    pub fn new(items: Vec<Item>) -> Self {
        Self { items }
    }

    pub fn sort(&mut self, srtmode: &Option<SortingMode>) {
        if let Some(srtmode) = srtmode {
            match srtmode {
                SortingMode::Abc(Course::Forward) => self.sort_abc_forward(),
                SortingMode::Abc(Course::Backward) => self.sort_abc_backward(),
                SortingMode::Mtime(Course::Forward) => self.sort_mtime_forward(),
                SortingMode::Mtime(Course::Backward) => self.sort_mtime_backward(),
                SortingMode::Size(Course::Forward) => self.sort_size_forward(),
                SortingMode::Size(Course::Backward) => self.sort_size_backward(),
            }
        }
    }

    fn sort_abc_forward(&mut self) {
        self.items
            .sort_unstable_by(|a, b| alphanumeric_sort::compare_path(&a.path, &b.path))
    }

    fn sort_abc_backward(&mut self) {
        self.items
            .sort_unstable_by(|a, b| alphanumeric_sort::compare_path(&b.path, &a.path))
    }

    fn sort_mtime_forward(&mut self) {
        self.items.sort_unstable_by(|a, b| a.mtime.cmp(&b.mtime));
    }

    fn sort_mtime_backward(&mut self) {
        self.items.sort_unstable_by(|a, b| b.mtime.cmp(&a.mtime));
    }

    fn sort_size_forward(&mut self) {
        self.items.sort_unstable_by(|a, b| a.size.cmp(&b.size));
    }

    fn sort_size_backward(&mut self) {
        self.items.sort_unstable_by(|a, b| b.size.cmp(&a.size));
    }
}
