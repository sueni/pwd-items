use std::ffi::OsString;
use std::path::PathBuf;

pub enum Course {
    Forward,
    Backward,
}

pub enum SortingMode {
    Abc(Course),
    Mtime(Course),
    Size(Course),
}

impl TryFrom<OsString> for SortingMode {
    type Error = ();

    fn try_from(value: OsString) -> Result<Self, Self::Error> {
        match value.to_str() {
            Some("abc-frw") => Ok(Self::Abc(Course::Forward)),
            Some("abc-bkwr") => Ok(Self::Abc(Course::Backward)),
            Some("mtime-frw") => Ok(Self::Mtime(Course::Forward)),
            Some("mtime-bkwr") => Ok(Self::Mtime(Course::Backward)),
            Some("size-frw") => Ok(Self::Size(Course::Forward)),
            Some("size-bkwr") => Ok(Self::Size(Course::Backward)),
            _ => Err(()),
        }
    }
}

pub enum Ftype {
    All,
    Files,
    DirsSmart,
    DirsAll,
}

impl TryFrom<OsString> for Ftype {
    type Error = ();

    fn try_from(value: OsString) -> Result<Self, Self::Error> {
        match value.to_str() {
            Some("a") => Ok(Self::All),
            Some("f") => Ok(Self::Files),
            Some("d") => Ok(Self::DirsSmart),
            Some(".d") => Ok(Self::DirsAll),
            _ => Err(()),
        }
    }
}

pub struct Args {
    pub srtmode: Option<SortingMode>,
    pub ftype: Ftype,
    pub dir: PathBuf,
}

pub const USAGE: &str = "Usage: pwd-items FTYPE DIRECTORY [--sorting=MODE]\
          \nSorting modes: <abc-frw | bc-bkwr | mtime-frw | mtime-bkwr | size-frw | size-bkwr>\
          \nFtypes: <a: everything | f: files | d: directories (smart) | .d: directories (all)>";

pub fn parse_args() -> Result<Args, lexopt::Error> {
    use lexopt::prelude::*;

    let mut srtmode = None;
    let mut ftype = None;
    let mut dir = std::path::PathBuf::from(".");
    let mut parser = lexopt::Parser::from_env();

    while let Some(arg) = parser.next()? {
        match arg {
            Long("sorting") => srtmode = parser.value()?.try_into().ok(),
            Value(val) if ftype.is_none() => ftype = val.try_into().ok(),
            Value(val) if ftype.is_some() => dir = PathBuf::from(val),
            Short('h') => {
                println!("{}", USAGE);
                std::process::exit(0);
            }
            _ => return Err(arg.unexpected()),
        }
    }

    let ftype = match ftype {
        None => return Err("Error: need an ftype argument to run".into()),
        Some(it) => it,
    };

    if !dir.is_dir() {
        return Err(format!("invalid directory: {}", dir.display()).into());
    }

    Ok(Args {
        srtmode,
        ftype,
        dir,
    })
}
