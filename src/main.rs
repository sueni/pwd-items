mod cli;
mod item;
mod printer;
mod sorter;

use cli::Ftype;
use cli::SortingMode;
use item::Item;
use sorter::Sorter;
use std::path::Path;

fn list_everything(path: &Path, srtmode: &Option<SortingMode>) {
    let everything = collect_items(path, &is_anything);
    process_items(everything, srtmode);
}

fn list_directories_smart(path: &Path, srtmode: &Option<SortingMode>) {
    let mut dirs = collect_items(path, &is_visible_dir);
    if dirs.is_empty() {
        dirs = collect_items(path, &is_any_dir);
    }
    process_items(dirs, srtmode);
}

fn list_all_directories(path: &Path, srtmode: &Option<SortingMode>) {
    let dirs = collect_items(path, &is_any_dir);
    process_items(dirs, srtmode);
}

fn list_files(path: &Path, srtmode: &Option<SortingMode>) {
    let files = collect_items(path, &is_file);
    process_items(files, srtmode);
}

fn collect_items(path: &Path, f: &dyn Fn(&Path) -> bool) -> Vec<Item> {
    path.read_dir()
        .expect("Could not read directory")
        .flatten()
        .map(|entry| entry.path())
        .filter(|path| f(path))
        .map(Item::new)
        .collect()
}

fn process_items(items: Vec<Item>, srtmode: &Option<SortingMode>) {
    let mut sorter = Sorter::new(items);
    sorter.sort(srtmode);
    printer::print(&sorter.items).expect("Failed to print");
}

fn is_file(path: &Path) -> bool {
    path.is_file()
}

fn is_visible_dir(path: &Path) -> bool {
    if !path.is_dir() {
        return false;
    }
    if let Ok(dir) = path.strip_prefix("./") {
        return !dir.to_str().map(|d| d.starts_with('.')).unwrap_or(true);
    }
    true
}

fn is_any_dir(path: &Path) -> bool {
    path.is_dir()
}

fn is_anything(_: &Path) -> bool {
    true
}

fn main() {
    let args = match cli::parse_args() {
        Ok(args) => args,
        Err(e) => {
            eprintln!("ERROR: {}\n{}", e, cli::USAGE);
            std::process::exit(1);
        }
    };

    match &args.ftype {
        Ftype::All => list_everything(&args.dir, &args.srtmode),
        Ftype::DirsSmart => list_directories_smart(&args.dir, &args.srtmode),
        Ftype::DirsAll => list_all_directories(&args.dir, &args.srtmode),
        Ftype::Files => list_files(&args.dir, &args.srtmode),
    }
}
